<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->primary('id');
        });
        DB::statement('CREATE SEQUENCE IF NOT EXISTS roles_id_seq OWNED BY roles.id;');
        DB::statement("SELECT SETVAL('roles_id_seq', (select max(id) + 1 from roles), false);");
        DB::statement("ALTER TABLE roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq');");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->dropPrimary('id');
            $table->unsignedInteger('id')->change();
        });
        DB::statement('DROP SEQUENCE IF EXISTS roles_id_seq;');
    }
};
