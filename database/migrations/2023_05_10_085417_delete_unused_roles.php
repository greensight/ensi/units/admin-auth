<?php

use Database\Helpers\RevokeRightsMigration;

return new class () extends RevokeRightsMigration {
    protected function roles(): array
    {
        return [101, 102];
    }

    protected function rights(): array
    {
        return [1202, 1203, 1204, 1205];
    }
};
