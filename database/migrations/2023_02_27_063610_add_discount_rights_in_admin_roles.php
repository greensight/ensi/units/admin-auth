<?php

use Database\Helpers\RightsMigration;

return new class () extends RightsMigration {
    protected function roles(): array
    {
        return [101, 102];
    }

    protected function rights(): array
    {
        return [
            1301, 1302, 1303, 1304, 1305, 1306,
            1311, 1312, 1313, 1314, 1315,
        ];
    }
};
