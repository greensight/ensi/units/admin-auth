<?php

use Database\Helpers\RightsMigration;

return new class () extends RightsMigration {
    protected function roles(): array
    {
        return [101, 102];
    }

    protected function rights(): array
    {
        return [1101, 1102, 1103, 1104, 1105, 1106];
    }
};
