<?php

use Database\Helpers\RightsMigration;

return new class () extends RightsMigration {
    protected function roles(): array
    {
        return [101, 102];
    }

    protected function rights(): array
    {
        return [416, 417, 1601, 1602, 1603, 1604, 1605];
    }
};
