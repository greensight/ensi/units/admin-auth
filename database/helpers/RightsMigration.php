<?php

namespace Database\Helpers;

use DB;
use Illuminate\Database\Migrations\Migration;

abstract class RightsMigration extends Migration
{
    abstract protected function roles(): array;

    abstract protected function rights(): array;

    public function up()
    {
        $this->assign();
    }

    public function down()
    {
        $this->revoke();
    }

    protected function assign()
    {
        foreach ($this->roles() as $roleId) {
            $role = $this->findRole($roleId);
            if (!$role) {
                continue;
            }
            $existRights = json_decode($role->rights_access, true);
            $this->updateRoleRights($roleId, array_merge($existRights, $this->rights()));
        }
    }

    protected function revoke()
    {
        foreach ($this->roles() as $roleId) {
            $role = $this->findRole($roleId);
            if (!$role) {
                continue;
            }
            $existRights = json_decode($role->rights_access, true);
            $this->updateRoleRights($roleId, array_diff($existRights, $this->rights()));
        }
    }

    protected function findRole(int $id): ?object
    {
        return DB::table('roles')->find($id);
    }

    protected function updateRoleRights(int $id, array $rights): void
    {
        sort($rights);
        DB::table('roles')->where('id', $id)->update([
            'rights_access' => json_encode(array_unique($rights)),
            'updated_at' => now(),
        ]);
    }
}
