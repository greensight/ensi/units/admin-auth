<?php

use App\Domain\Kafka\Actions\Send\SendKafkaMessageAction;
use App\Domain\Users\Models\User;
use App\Domain\Users\Models\UserRole;
use App\Http\ApiV1\OpenApiGenerated\Enums\RoleEnum;

use function Pest\Laravel\artisan;
use function Pest\Laravel\assertDatabaseHas;

use Tests\ComponentTestCase;
use Tests\IntegrationTestCase;

uses(ComponentTestCase::class);
uses()->group('component');

test("Command passwords:deactivate-token success", function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');

    /** @var User $user */
    $user = User::factory()->create();
    $user->password_token_created_at = now()->subDays(User::LIFE_TIME_PASSWORD_TOKEN + 1);
    $user->save();

    artisan("passwords:deactivate-token");

    assertDatabaseHas(User::class, [
        'id' => $user->id,
        'password_token' => null,
        'password_token_created_at' => null,
    ]);
});

test("Command user:create-with-role {login} {password} {role=102} success", function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');

    $faker = Faker\Factory::create();
    $login = $faker->userName();
    $password = $faker->password();
    $roleId = RoleEnum::SUPER_ADMIN->value;

    artisan("user:create-with-role {$login} {$password} {$roleId}");

    /** @var User $user */
    $user = User::query()->where('login', $login)->first();

    assertDatabaseHas(UserRole::class, [
        'user_id' => $user->id,
        'role_id' => $roleId,
    ]);
});
