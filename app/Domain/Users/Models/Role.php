<?php

namespace App\Domain\Users\Models;

use App\Domain\Users\Tests\Factories\RoleFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\RightsAccessEnum;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 * @property bool $active
 * @property array|RightsAccessEnum[] $rights_access
 *
 * @property-read UserRole|null $pivot
 */
class Role extends Model
{
    protected $table = 'roles';

    protected $fillable = ['title', 'active', 'rights_access'];

    protected $casts = [
        'rights_access' => 'array',
    ];

    public static function factory(): RoleFactory
    {
        return RoleFactory::new();
    }
}
