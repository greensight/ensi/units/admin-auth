<?php

namespace App\Domain\Users\Observers;

use App\Domain\Kafka\Actions\Send\Events\SendUserEventAction;
use App\Domain\Kafka\Actions\Send\SendGeneratedPasswordTokenAction;
use App\Domain\Kafka\Messages\Send\ModelEventMessage;
use App\Domain\Users\Models\User;

class UserObserver
{
    public function __construct(
        protected SendGeneratedPasswordTokenAction $sendGeneratedPasswordTokenAction,
        protected SendUserEventAction $sendUserEventAction,
    ) {
    }

    public function creating(User $user): void
    {
        $user->generatePasswordToken();
    }

    public function created(User $user): void
    {
        $this->sendGeneratedPasswordTokenAction->execute($user);
    }

    public function updating(User $user): void
    {
        if ($user->isDirty('password')) {
            $user->destroyPasswordToken();
        }
    }

    public function updated(User $user): void
    {
        if ($user->isActive() && !$user->isDirty('password_token')) {
            $this->sendUserEventAction->execute($user, ModelEventMessage::UPDATE);
        }
    }
}
