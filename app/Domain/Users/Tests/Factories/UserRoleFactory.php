<?php

namespace App\Domain\Users\Tests\Factories;

use App\Domain\Users\Models\Role;
use App\Domain\Users\Models\User;
use App\Domain\Users\Models\UserRole;
use Ensi\LaravelTestFactories\BaseModelFactory;

/** @extends BaseModelFactory<UserRole> */
class UserRoleFactory extends BaseModelFactory
{
    protected $model = UserRole::class;

    public function definition(): array
    {
        return [
            'user_id' => User::factory(),
            'role_id' => Role::factory(),
        ];
    }

    public function fastCreate(User $user, Role $role): UserRole
    {
        return $this->create(['user_id' => $user->id, 'role_id' => $role->id]);
    }
}
