<?php

namespace App\Domain\Users\Tests\Factories;

use App\Domain\Users\Models\Role;
use App\Http\ApiV1\OpenApiGenerated\Enums\RightsAccessEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

class RoleFactory extends BaseModelFactory
{
    protected $model = Role::class;

    public function definition(): array
    {
        return [
            'id' => $this->faker->unique()->numberBetween(1000, 2000),
            'active' => $this->faker->boolean(),
            'title' => $this->faker->unique()->title(),
            'rights_access' => $this->faker->randomElements(
                RightsAccessEnum::cases(),
                $this->faker->numberBetween(1, 3)
            ),
        ];
    }

    public function active($active = true): static
    {
        return $this->state(['active' => $active]);
    }
}
