<?php

namespace App\Domain\Users\Actions;

use App\Domain\Users\Models\User;

class CreateUserAction
{
    public function execute(array $fields): User
    {
        $user = new User();
        $user->fill($fields);
        $user->save();

        return $user;
    }
}
