<?php

namespace App\Domain\Users\Actions;

use App\Domain\Users\Models\Role;

class DeleteRoleAction
{
    public function execute(int $id): void
    {
        $role = Role::query()->findOrFail($id);
        $role->delete();
    }
}
