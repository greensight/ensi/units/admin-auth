<?php

namespace App\Domain\Users\Actions;

use App\Domain\Kafka\Actions\Send\SendGeneratedPasswordTokenAction;
use App\Domain\Users\Models\User;

class RefreshPasswordTokenAction
{
    public function __construct(protected SendGeneratedPasswordTokenAction $sendGeneratedPasswordTokenAction)
    {
    }

    public function execute(int $userId): void
    {
        /** @var User $user */
        $user = User::query()->findOrFail($userId);
        $user->generatePasswordToken();
        $user->save();

        $this->sendGeneratedPasswordTokenAction->execute($user);
    }
}
