<?php

namespace App\Domain\Users\Actions;

use App\Domain\Users\Models\User;

class DeleteUserAction
{
    public function execute(int $id): void
    {
        $user = User::query()->findOrFail($id);
        $user->delete();
    }
}
