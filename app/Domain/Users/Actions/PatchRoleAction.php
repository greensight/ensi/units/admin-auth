<?php

namespace App\Domain\Users\Actions;

use App\Domain\Users\Models\Role;

class PatchRoleAction
{
    public function execute(int $id, array $fields): Role
    {
        /** @var Role $role */
        $role = Role::query()->findOrFail($id);
        $role->update($fields);

        return $role;
    }
}
