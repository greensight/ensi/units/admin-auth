<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\Events\DeactivatedUserMessage;

class SendDeactivatedUserAction
{
    public function __construct(protected readonly SendKafkaMessageAction $sendAction)
    {
    }

    public function execute(int $userId, string $causeDeactivation): void
    {
        $event = new DeactivatedUserMessage($userId, $causeDeactivation);
        $this->sendAction->execute($event);
    }
}
