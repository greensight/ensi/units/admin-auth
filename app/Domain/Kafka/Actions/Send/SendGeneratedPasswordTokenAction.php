<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\Events\PasswordTokenMessage;
use App\Domain\Users\Models\User;

class SendGeneratedPasswordTokenAction
{
    public function __construct(protected readonly SendKafkaMessageAction $sendAction)
    {
    }

    public function execute(User $user): void
    {
        $event = new PasswordTokenMessage($user);
        $this->sendAction->execute($event);
    }
}
