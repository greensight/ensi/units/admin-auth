<?php

namespace App\Domain\Kafka\Messages\Send\Events;

use App\Domain\Kafka\Messages\Send\KafkaMessage;
use App\Domain\Users\Models\User;

class PasswordTokenMessage extends KafkaMessage
{
    public function __construct(protected User $user)
    {
    }

    public function toArray(): array
    {
        return [
            'user_id' => $this->user->id,
            'token' => $this->user->password_token,
        ];
    }

    public function topicKey(): string
    {
        return 'generated-password-token';
    }
}
