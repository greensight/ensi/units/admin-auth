<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvents;

use App\Domain\Kafka\Messages\Send\Payload;
use App\Domain\Users\Models\User;

class UserPayload extends Payload
{
    public function __construct(protected User $user)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->user->id,
            'active' => $this->user->active,
            'full_name' => $this->user->full_name,
            'phone' => $this->user->phone,
            'email' => $this->user->email,
            'password_token' => $this->user->password_token,
        ];
    }
}
