<?php

namespace App\Http\ApiV1\Support\Rules;

use Illuminate\Contracts\Validation\Rule;
use Validator;

class StrictEmail implements Rule
{
    public function passes($attribute, $value)
    {
        $validator = Validator::make([$attribute => $value], [
            $attribute => 'email',
        ]);

        if ($validator->fails()) {
            return false;
        }

        list($localPart, $domainPart) = explode('@', $value);

        if (strpos($domainPart, '[') === 0 && substr($domainPart, -1) === ']') {
            $domainPart = substr($domainPart, 1, -1);
        }

        return preg_match('/^[^\s@]+@[^\s@]+\.[^\s@]+$/', $value) &&
               !filter_var($domainPart, FILTER_VALIDATE_IP);
    }

    public function message()
    {
        return __('validation.email');
    }
}
