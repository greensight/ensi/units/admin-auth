<?php

use App\Domain\Users\Models\Role;
use App\Http\ApiV1\Modules\Users\Tests\Factories\RoleRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/roles 201', function () {
    $roleData = RoleRequestFactory::new()->make();

    postJson('/api/v1/roles', $roleData)
        ->assertStatus(201)
        ->assertJsonPath('data.title', $roleData['title']);

    $roleData["rights_access"] = json_encode($roleData["rights_access"]);

    assertDatabaseHas(Role::class, $roleData);
});

test('POST /api/v1/roles 400', function () {
    $roleData = RoleRequestFactory::new()->make(['title' => 'admin']);

    postJson('/api/v1/roles', $roleData)
        ->assertStatus(400)
        ->assertJsonPath('errors.0.code', "ValidationError");
});

test('GET /api/v1/roles/{id} 200', function () {
    $role = Role::factory()->createOne();

    getJson("/api/v1/roles/$role->id")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $role->id);
});

test('GET /api/v1/roles/{id} 404', function () {
    getJson("/api/v1/roles/1000")
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test('PATCH /api/v1/roles/{id} 200', function () {
    /** @var Role $role */
    $role = Role::factory()->create();
    $id = $role->id;

    $newRoleData = RoleRequestFactory::new()->only(['title'])->make(['title' => 'just_user']);

    patchJson("/api/v1/roles/$id", $newRoleData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $id)
        ->assertJsonPath('data.title', $newRoleData['title'])
        ->assertJsonPath('data.rights_access', $role->rights_access);

    assertDatabaseHas(Role::class, [
        'id' => $id,
        'title' => $newRoleData['title'],
        'rights_access' => json_encode($role->rights_access),
    ]);
});

test('PATCH /api/v1/roles/{id} 404', function () {
    patchJson('/api/v1/roles/1000', RoleRequestFactory::new()->make())
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test('DELETE /api/v1/roles/{id} success', function () {
    /** @var Role $role */
    $role = Role::factory()->create();
    $id = $role->id;

    deleteJson("/api/v1/roles/$id")->assertStatus(200);

    assertModelMissing($role);
});

test('POST /api/v1/roles:search 200', function () {
    $roles = Role::factory()->count(3)->create();
    $lastId = $roles->sortBy('id')->last()->id;

    $requestBody = [
        "sort" => [
            "-id",
        ],
    ];

    postJson("/api/v1/roles:search", $requestBody)
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $lastId);
});

test("POST /api/v1/roles:search filter success", function (
    string $fieldKey,
    mixed $value,
    ?string $filterKey,
    $filterValue,
) {
    /** @var Role $role */
    $role = Role::factory()->create($value ? [$fieldKey => $value] : []);
    Role::factory()->create(is_bool($value) ? [$fieldKey => !$value] : []);

    postJson("/api/v1/roles:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $role->{$fieldKey}),
    ], 'sort' => ['-id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $role->id);
})->with([
    ['id', null, null, null],
    ['active', true, null, null],
    ['title', 'role name', 'title_like', "nam"],
]);

test("POST /api/v1/roles:search filter range date success", function (string $fieldKey) {
    /** @var Role $role */
    $role = Role::factory()->create([$fieldKey => '2022-05-05 11:25:14']);
    Role::factory()->create();

    $request = [
        'filter' => [
            "{$fieldKey}_gte" => '2022-05-05T10:00:00.0Z',
            "{$fieldKey}_lte" => '2022-05-05T14:00:00.0Z',
        ],
    ];

    postJson("/api/v1/roles:search", $request)
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $role->id);
})->with([
    ['created_at'],
    ['updated_at'],
]);

test("POST /api/v1/roles:search sort success", function (string $sort) {
    Role::factory()->create();
    postJson("/api/v1/roles:search", ["sort" => [$sort]])->assertOk();
})->with([
    'id', 'title', 'created_at', 'updated_at',
]);
