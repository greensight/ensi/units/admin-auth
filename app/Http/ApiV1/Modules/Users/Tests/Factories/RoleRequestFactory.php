<?php

namespace App\Http\ApiV1\Modules\Users\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\RightsAccessEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class RoleRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'active' => $this->faker->boolean(),
            'title' => $this->faker->title(),
            'rights_access' => $this->faker->randomElements(
                RightsAccessEnum::cases(),
                $this->faker->numberBetween(1, 3)
            ),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
