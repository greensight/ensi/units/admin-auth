<?php


use App\Domain\Kafka\Actions\Send\SendKafkaMessageAction;
use App\Domain\Users\Models\Role;
use App\Domain\Users\Models\User;
use App\Domain\Users\Models\UserRole;
use App\Http\ApiV1\OpenApiGenerated\Enums\GrantTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Laravel\Passport\Client;
use Laravel\Passport\Token;
use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Token\Parser;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\postJson;

use Tests\IntegrationTestCase;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'oauth');

test('POST /api/v1/oauth/token success', function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');

    $client = Client::factory()->asPasswordClient()->create();

    $password = 'Test1234!';
    $user = User::factory()->active()->create(['password' => $password]);
    $role = Role::factory()->active()->create();
    UserRole::factory()->fastCreate($user, $role);

    $requestBody = [
        "grant_type" => GrantTypeEnum::PASSWORD,
        "client_id" => $client['id'],
        "client_secret" => $client['secret'],
        "scope" => [],
        "username" => $user->login,
        "password" => $password,
    ];

    postJson('/api/v1/oauth/token', $requestBody)
        ->assertStatus(200);

    assertDatabaseHas(Token::class, [
        'user_id' => $user['id'],
        'client_id' => $client['id'],
    ]);
});

test('POST /api/v1/oauth/token incorrect credentials', function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');

    $client = Client::factory()->asPasswordClient()->create();

    $correctPassword = 'Test1234!';
    $wrongPassword = 'Test1234!*';
    /** @var User $user */
    $user = User::factory()->active()->create(['password' => $correctPassword]);
    $role = Role::factory()->active()->create();
    UserRole::factory()->fastCreate($user, $role);

    $requestBody = [
        "grant_type" => GrantTypeEnum::PASSWORD,
        "client_id" => $client['id'],
        "client_secret" => $client['secret'],
        "scope" => [],
        "username" => $user->login,
        "password" => $wrongPassword,
    ];

    postJson('/api/v1/oauth/token', $requestBody)
        ->assertStatus(401)
        ->assertJsonPath(
            'errors.0.message',
            __('override-error-message.Laravel\Passport\Exceptions\OAuthServerException.6')
        );
});

test('POST /api/v1/oauth/token not active user', function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');

    $client = Client::factory()->asPasswordClient()->create();

    $correctPassword = 'Test1234!';

    $user = User::factory()->active(false)->create(['password' => $correctPassword]);
    $role = Role::factory()->active()->create();
    UserRole::factory()->fastCreate($user, $role);

    $requestBody = [
        "grant_type" => GrantTypeEnum::PASSWORD,
        "client_id" => $client['id'],
        "client_secret" => $client['secret'],
        "scope" => [],
        "username" => $user->login,
        "password" => $correctPassword,
    ];

    postJson('/api/v1/oauth/token', $requestBody)
        ->assertStatus(401)
        ->assertJsonPath(
            'errors.0.message',
            __('override-error-message.Laravel\Passport\Exceptions\OAuthServerException.1000')
        );
});

test('POST /api/v1/oauth/token not active roles', function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');

    $client = Client::factory()->asPasswordClient()->create();

    $correctPassword = 'Test1234!';
    /** @var User $user */
    $user = User::factory()->active()->create(['password' => $correctPassword]);

    $role = Role::factory()->active(false)->create();
    UserRole::factory()->fastCreate($user, $role);

    $requestBody = [
        "grant_type" => GrantTypeEnum::PASSWORD,
        "client_id" => $client['id'],
        "client_secret" => $client['secret'],
        "scope" => [],
        "username" => $user->login,
        "password" => $correctPassword,
    ];

    postJson('/api/v1/oauth/token', $requestBody)
        ->assertStatus(401)
        ->assertJsonPath(
            'errors.0.message',
            __('override-error-message.Laravel\Passport\Exceptions\OAuthServerException.1001')
        );
});

test('DELETE /api/v1/oauth/tokens/{id} success', function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');

    $client = Client::factory()->asPasswordClient()->create();

    $password = 'Test1234!';
    $user = User::factory()->active()->create(['password' => $password]);

    $role = Role::factory()->active()->create();
    UserRole::factory()->fastCreate($user, $role);

    $requestBody = [
        "grant_type" => GrantTypeEnum::PASSWORD,
        "client_id" => $client['id'],
        "client_secret" => $client['secret'],
        "scope" => [],
        "username" => $user->login,
        "password" => $password,
    ];

    $testResponse = postJson('/api/v1/oauth/token', $requestBody);
    $token = json_decode($testResponse->baseResponse->content())->access_token;
    /** @phpstan-ignore-next-line */
    $tokenId = (new Parser(new JoseEncoder()))->parse($token)->claims()->all()['jti'];

    deleteJson(
        "/api/v1/oauth/tokens/$tokenId",
        [],
        ['Authorization' => 'Bearer ' . $token]
    )
        ->assertStatus(204);

    assertDatabaseHas(Token::class, [
        'id' => $tokenId,
        'revoked' => true,
    ]);
});

test('DELETE /api/v1/oauth/tokens/{id} 404', function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendKafkaMessageAction::class)->shouldReceive('execute');

    $client = Client::factory()->asPasswordClient()->create();

    $password = 'Test1234!';
    $user = User::factory()->active()->create(['password' => $password]);

    $role = Role::factory()->active()->create();
    UserRole::factory()->fastCreate($user, $role);

    $requestBody = [
        "grant_type" => GrantTypeEnum::PASSWORD,
        "client_id" => $client['id'],
        "client_secret" => $client['secret'],
        "scope" => [],
        "username" => $user->login,
        "password" => $password,
    ];

    $testResponse = postJson('/api/v1/oauth/token', $requestBody);

    $token = json_decode($testResponse->baseResponse->content())->access_token;
    $tokenId = '123';

    deleteJson(
        "/api/v1/oauth/tokens/$tokenId",
        [],
        ['Authorization' => 'Bearer ' . $token]
    )
        ->assertStatus(404);
});
