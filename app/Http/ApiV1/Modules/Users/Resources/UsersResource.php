<?php

namespace App\Http\ApiV1\Modules\Users\Resources;

use App\Domain\Users\Models\User;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin User */
class UsersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'login' => $this->login,
            'active' => $this->active,
            'last_name' => $this->last_name,
            'first_name' => $this->first_name,
            'middle_name' => $this->middle_name,
            'full_name' => $this->full_name,
            'short_name' => $this->short_name,
            'email' => $this->email,
            'phone' => $this->phone,
            'timezone' => $this->timezone,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'roles' => RolesResource::collection($this->whenLoaded('roles')),
        ];
    }
}
