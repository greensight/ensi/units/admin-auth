<?php

namespace App\Http\ApiV1\Modules\Users\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

class RightsAccessResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return $this->resource;
    }
}
