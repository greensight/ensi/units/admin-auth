<?php

namespace App\Http\ApiV1\Modules\Users\Queries;

use App\Domain\Users\Models\User;
use App\Http\ApiV1\Modules\Users\Filters\FiltersUserFullName;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class UsersQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(User::query());

        $this->allowedSorts(['id', 'email', 'phone', 'active', 'login', 'created_at', 'updated_at']);
        $this->allowedIncludes(['roles']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('login'),
            AllowedFilter::exact('password_token'),
            AllowedFilter::exact('active'),
            AllowedFilter::exact('role', 'roles.id'),

            ...StringFilter::make('phone')->exact()->contain(),
            ...StringFilter::make('email')->exact()->contain(),

            AllowedFilter::custom('full_name', new FiltersUserFullName()),
            ...StringFilter::make('last_name')->contain(),
            ...StringFilter::make('first_name')->contain(),
            ...StringFilter::make('middle_name')->contain(),

            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);

        $this->defaultSort('id');
    }
}
