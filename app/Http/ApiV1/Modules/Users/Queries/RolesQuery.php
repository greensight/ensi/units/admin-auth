<?php

namespace App\Http\ApiV1\Modules\Users\Queries;

use App\Domain\Users\Models\Role;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class RolesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Role::query());

        $this->allowedSorts(['id', 'title', 'created_at', 'updated_at']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('active'),
            ...StringFilter::make('title')->contain(),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);

        $this->defaultSort('id');
    }
}
