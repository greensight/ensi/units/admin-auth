<?php

namespace App\Http\ApiV1\Modules\Users\Requests;

use App\Domain\Users\Models\User;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use App\Http\ApiV1\Support\Rules\StrictEmail;
use Illuminate\Validation\Rule;

class PatchUserRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $id = (int) $this->route('userId');

        return [
            'login' => [Rule::unique(User::class)->ignore($id)],
            'active' => ['boolean'],
            'cause_deactivation' => ['sometimes', 'required_if:active,false', 'string'],
            'password' => ['nullable'],
            'first_name' => ['nullable'],
            'last_name' => ['nullable'],
            'middle_name' => ['nullable'],
            'phone' => ['regex:/^\+7\d{10}$/', Rule::unique(User::class)->ignore($id)],
            'email' => [new StrictEmail(), Rule::unique(User::class)->ignore($id)],
            'timezone' => ['sometimes', 'required', 'timezone'],
        ];
    }
}
