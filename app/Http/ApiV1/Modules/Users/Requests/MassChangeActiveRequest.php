<?php

namespace App\Http\ApiV1\Modules\Users\Requests;

use App\Domain\Users\Actions\Data\MassChangeActiveData;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class MassChangeActiveRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'user_ids' => ['required', 'array'],
            'user_ids.*' => ['integer'],
            'active' => ['required', 'boolean'],
            'cause_deactivation' => ['required_if:active,false', 'string'],
        ];
    }

    public function convertToObject(): MassChangeActiveData
    {
        return new MassChangeActiveData(
            userIds: $this->input('user_ids'),
            active: $this->input('active'),
            causeDeactivation: $this->input('cause_deactivation'),
        );
    }
}
