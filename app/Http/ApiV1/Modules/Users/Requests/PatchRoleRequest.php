<?php

namespace App\Http\ApiV1\Modules\Users\Requests;

use App\Domain\Users\Models\Role;
use App\Http\ApiV1\OpenApiGenerated\Enums\RightsAccessEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class PatchRoleRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $id = (int) $this->route('id');

        return [
            'title' => ['nullable', 'string', Rule::unique(Role::class)->ignore($id)],
            'active' => ['nullable','boolean'],
            'rights_access' => ['array'],
            'rights_access.*' => ['integer', new Enum(RightsAccessEnum::class)],
        ];
    }
}
