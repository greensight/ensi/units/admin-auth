<?php

namespace App\Http\ApiV1\Modules\Users\Requests;

use App\Domain\Users\Models\Role;
use App\Http\ApiV1\OpenApiGenerated\Enums\RightsAccessEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class CreateRoleRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'title' => ['required', 'string', Rule::unique(Role::class)],
            'active' => ['boolean'],
            'rights_access' => ['present', 'array'],
            'rights_access.*' => ['integer', new Enum(RightsAccessEnum::class)],
        ];
    }
}
