<?php

namespace App\Http\ApiV1\Modules\Users\Controllers;

use App\Domain\Users\Actions\CreateRoleAction;
use App\Domain\Users\Actions\DeleteRoleAction;
use App\Domain\Users\Actions\PatchRoleAction;
use App\Http\ApiV1\Modules\Users\Queries\RolesQuery;
use App\Http\ApiV1\Modules\Users\Requests\CreateRoleRequest;
use App\Http\ApiV1\Modules\Users\Requests\PatchRoleRequest;
use App\Http\ApiV1\Modules\Users\Resources\RolesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class RolesController
{
    public function create(CreateRoleRequest $request, CreateRoleAction $action): Responsable
    {
        return new RolesResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchRoleRequest $request, PatchRoleAction $action): Responsable
    {
        return new RolesResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteRoleAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, RolesQuery $query): Responsable
    {
        return new RolesResource($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, RolesQuery $query): Responsable
    {
        return RolesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
