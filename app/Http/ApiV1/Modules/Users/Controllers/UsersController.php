<?php

namespace App\Http\ApiV1\Modules\Users\Controllers;

use App\Domain\Auth\Actions\LogoutIfAccessDenied;
use App\Domain\Users\Actions\CreateUserAction;
use App\Domain\Users\Actions\DeleteUserAction;
use App\Domain\Users\Actions\MassChangeActiveAction;
use App\Domain\Users\Actions\PatchUserAction;
use App\Domain\Users\Actions\RefreshPasswordTokenAction;
use App\Domain\Users\Models\User;
use App\Http\ApiV1\Modules\Users\Queries\UsersQuery;
use App\Http\ApiV1\Modules\Users\Requests\CreateUserRequest;
use App\Http\ApiV1\Modules\Users\Requests\MassChangeActiveRequest;
use App\Http\ApiV1\Modules\Users\Requests\PatchUserRequest;
use App\Http\ApiV1\Modules\Users\Resources\UsersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Request;

class UsersController
{
    public function create(CreateUserRequest $request, CreateUserAction $action): Responsable
    {
        return new UsersResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchUserRequest $request, PatchUserAction $action): Responsable
    {
        return new UsersResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteUserAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, UsersQuery $query): Responsable
    {
        return new UsersResource($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, UsersQuery $query): Responsable
    {
        return UsersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(UsersQuery $query): Responsable
    {
        return new UsersResource($query->firstOrFail());
    }

    public function current(Request $request, LogoutIfAccessDenied $logoutIfAccessDenied): Responsable
    {
        /** @var User|null $user */
        $user = $request->user();

        if ($user) {
            $user->loadMissing('roles');
            $logoutIfAccessDenied->execute($user);

            return new UsersResource($user);
        }

        return new EmptyResource();
    }

    public function refreshPasswordToken(int $usedId, RefreshPasswordTokenAction $action): Responsable
    {
        $action->execute($usedId);

        return new EmptyResource();
    }

    public function massChangeActive(MassChangeActiveRequest $request, MassChangeActiveAction $action): Responsable
    {
        $action->execute($request->convertToObject());

        return new EmptyResource();
    }
}
